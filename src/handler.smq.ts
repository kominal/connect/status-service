import { Packet } from '@kominal/service-util/model/packet';
import service from '.';
import { info, error, debug } from '@kominal/service-util/helper/log';
import StatusDatabase from '@kominal/connect-models/status/status.database';
import SocketConnectionDatabase from '@kominal/connect-models/socketconnection/socketconnection.status.database';

export class SMQHandler {
	async onConnect(): Promise<void> {
		info('Connected to SwarmMQ cluster.');
		const smqClient = service.getSMQClient();
		if (smqClient) {
			smqClient.subscribe('QUEUE', 'STATUS.SYNC');
			smqClient.subscribe('QUEUE', 'STATUS.CONNECT');
			smqClient.subscribe('QUEUE', 'STATUS.DISCONNECT');
			smqClient.subscribe('QUEUE', 'STATUS.REFRESH');
		}
	}

	async onDisconnect(): Promise<void> {
		error('Lost connection to SwarmMQ cluster.');
	}

	async onMessage(packet: Packet): Promise<void> {
		const { destinationType, destinationName, event } = packet;
		if (!event) {
			return;
		}
		const { type, body } = event;
		if (destinationType === 'QUEUE' && destinationName === 'STATUS.SYNC') {
			this.onSync(type, body);
		} else if (destinationType === 'QUEUE' && destinationName === 'STATUS.CONNECT') {
			this.onUserConnect(body);
		} else if (destinationType === 'QUEUE' && destinationName === 'STATUS.DISCONNECT') {
			this.onUserDisconnect(body);
		} else if (destinationType === 'QUEUE' && destinationName === 'STATUS.REFRESH') {
			this.onUserRefresh(body);
		}
	}

	async onSync(type: string, body: any) {
		if (type === 'SYNC') {
			const { taskIds } = body;
			info(`Starting sync for the following socket services: ${taskIds}`);
			const socketConnections = await SocketConnectionDatabase.find({ taskId: { $not: { $in: taskIds } } });
			for (const socketConnection of socketConnections) {
				this.onUserDisconnect({
					taskId: socketConnection.get('taskId'),
					connectionId: socketConnection.get('connectionId'),
					userId: socketConnection.get('userId'),
				});
			}
			const result = await SocketConnectionDatabase.deleteMany({ taskId: { $not: { $in: taskIds } } });
			if (result.deletedCount && result.deletedCount > 0) {
				info(`Removed ${result.deletedCount} dead socket connections!`);
			}
		} else if (type === 'RESET') {
			const taskId: string = body.taskId;
			const connections: { connectionId: string; userId: string }[] = body.connections;
			const connectionIds = connections.map((c) => c.connectionId);
			info(`Starting reset for the following socket service: ${taskId}, Connections: ${connectionIds}`);
			const socketConnections = await SocketConnectionDatabase.find({ taskId, connectionId: { $not: { $in: connectionIds } } });
			for (const socketConnection of socketConnections) {
				await this.onUserDisconnect({ taskId, connectionId: socketConnection.get('connectionId'), userId: socketConnection.get('userId') });
			}

			for (const connection of connections) {
				const { connectionId, userId } = connection;
				if ((await SocketConnectionDatabase.countDocuments({ taskId, connectionId, userId })) === 0) {
					await this.onUserConnect({ taskId, connectionId, userId, interestingUserIds: [] });
					service.getSMQClient()?.publish('TOPIC', `INTERNAL.REFRESH.${userId}`, 'REFRESH');
				}
			}
		}
	}

	async onUserRefresh(body: { userId: string; interestingUserIds: string[] }) {
		const smqClient = service.getSMQClient();
		if (smqClient) {
			const { userId, interestingUserIds } = body;
			for (const socketConnection of await SocketConnectionDatabase.find({ userId })) {
				const taskId = socketConnection.get('taskId');
				const connectionId = socketConnection.get('connectionId');

				const previousInterestingUserIds: string[] = socketConnection.get('interestingUserIds');
				const newInterestingUserIds = interestingUserIds.filter((g) => !previousInterestingUserIds.includes(g));
				const oldInterestingUserIds = previousInterestingUserIds.filter((g) => !interestingUserIds.includes(g));

				for (const userId of oldInterestingUserIds) {
					service.getSMQClient()?.publish('TOPIC', `DIRECT.CONNECTION.${connectionId}`, 'STATUS', { userId, status: 'OFFLINE' });
				}

				for (const userId of newInterestingUserIds) {
					const statusSetting = await StatusDatabase.findOne({ userId });
					const status = statusSetting?.get('status') || 'ONLINE';
					if (status != 'OFFLINE') {
						service.getSMQClient()?.publish('TOPIC', `DIRECT.CONNECTION.${connectionId}`, 'STATUS', { userId, status });
					}
				}

				await SocketConnectionDatabase.findOneAndUpdate({ taskId, connectionId }, { interestingUserIds });
			}
		}
	}

	async onUserConnect(body: { taskId: string; connectionId: string; userId: string; interestingUserIds: string[] }) {
		const { taskId, connectionId, userId, interestingUserIds } = body;

		debug(`Connection from user ${userId}, connection ${connectionId} established.`);

		await SocketConnectionDatabase.updateOne({ taskId, userId, connectionId }, { interestingUserIds }, { upsert: true });

		const statusSetting = await StatusDatabase.findOne({ userId });
		const status = statusSetting?.get('status') || 'ONLINE';

		let sent = false;
		if (status != 'OFFLINE' && (await SocketConnectionDatabase.countDocuments({ userId })) === 1) {
			service.getSMQClient()?.publish('TOPIC', `PUBLIC.USER.${userId}`, 'STATUS', { userId, status });
			sent = true;
		}

		const interestingConnections = await SocketConnectionDatabase.find({ userId: { $in: interestingUserIds } });
		const interestingUsers: string[] = [];
		for (const socketConnection of interestingConnections) {
			const socketUserId = String(socketConnection.get('userId'));
			if ((!sent || socketUserId != userId) && !interestingUsers.includes(socketUserId)) {
				interestingUsers.push(socketUserId);
			}
		}

		for (const socketUserId of interestingUsers) {
			const statusSetting = await StatusDatabase.findOne({ userId: socketUserId });
			const status = statusSetting?.get('status') || 'ONLINE';
			if (status != 'OFFLINE') {
				console.log('Sending status for', socketUserId, status);
				service.getSMQClient()?.publish('TOPIC', `DIRECT.CONNECTION.${connectionId}`, 'STATUS', { userId: socketUserId, status });
			}
		}
	}

	async onUserDisconnect(payload: { taskId: string; connectionId: string; userId: string }) {
		const { taskId, connectionId, userId } = payload;

		debug(`Connection from user ${userId}, connection ${connectionId} lost.`);

		const statusSetting = await StatusDatabase.findOne({ userId });
		const status = statusSetting?.get('status') || 'ONLINE';

		await SocketConnectionDatabase.deleteMany({ taskId, userId, connectionId });

		if (status != 'OFFLINE' && (await SocketConnectionDatabase.countDocuments({ userId })) === 0) {
			service.getSMQClient()?.publish('TOPIC', `PUBLIC.USER.${userId}`, 'STATUS', { userId, status });
		}
	}
}
