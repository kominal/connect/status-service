import Service from '@kominal/service-util/helper/service';
import status from './routes/status';
import { SMQHandler } from './handler.smq';

const service = new Service({
	id: 'status-service',
	name: 'Status Service',
	description: 'Enables users to share there current status with each other.',
	jsonLimit: '16mb',
	routes: [status],
	database: true,
	squad: true,
	swarmMQ: new SMQHandler(),
});
service.start();

export default service;
