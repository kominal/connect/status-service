# Call Service

The status service is used to share a live status between clients.

## Documentation

Test: https://connect-test.kominal.com/status-service/api-docs
Production: https://connect.kominal.com/status-service/api-docs
